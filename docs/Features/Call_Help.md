# Call For Help

GitMate can predict which contributors have the most experience with a certain part of the project or a specific kind of issue, based on their previous involvement.

To use this feature activate `Ping people with experience` setting of the `Find similar issues` plugin.

![image](../images/call_help_plugin.png)

By mentioning appropriate contributors in the issue discussion, GitMate notifies them of the issues they need to know about.

![image](../images/duplicate_issues.png)

To exclude contributors from the prediction provide a comma separated list of excluded contributors.
